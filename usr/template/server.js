'use strict';
const http = require('http');
const hostname = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '0.0.0.0';
const port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;

http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello World\n');
}).listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});